package edu.upc.damo.toDoList;

import java.util.Iterator;

/**
 * Created by Josep M on 24/10/2014.
 */
public class ModelObservable implements Iterable<CharSequence> {
    // Comportament d'observable

    public interface OnCanviModelListener {
        public void onNovesDades();
    }

    private OnCanviModelListener observador;
    private Model model;

    public ModelObservable(Model model) {
        this.model = model;
    }

    public void setOnCanviModelListener(OnCanviModelListener observador) {
        this.observador = observador;
    }

    private void avisaObservador() {
        if (observador!=null)
            observador.onNovesDades();
    }

    // Delegació al model embolcallat

    public void add(CharSequence s) {
        model.add(s);
        avisaObservador();
    }


    public void del(int pos) {
        model.del(pos);
        avisaObservador();
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return model.iterator();
    }


}

