package edu.upc.damo.toDoList;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by Josep M on 15/10/2014.
 */
public class AdapterMeu implements ListAdapter {
    Model dades;

    Context c;
    int resource;       // Layout de fila
    int textResourceId;   // TextView dins del layout de fila on s'ha de visualitzar cada dada

    AdapterMeu(Context c, int resource, int textResourceId, Model dades) {
        this.c=c;
        this.resource = resource;
        this.textResourceId = textResourceId;
        this.dades = dades;
    }




    @Override
    public int getCount() {
        return dades.size();
    }

    @Override
    public Object getItem(int position) {
        return dades.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return dades.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null)
        {
            convertView = ((Activity) c).getLayoutInflater().inflate(resource,null);
        }

        TextView textView = convertView.findViewById(textResourceId);
        String dada = dades.getItem(position).toString();
        textView.setText(dada);
        return convertView;
    }


    // listView.setAdapter() exigeix un ListAdapter. Aquest no té la separació de responsabilitats que volem,
    // i això ens exigeix a definir aquestes funcions que no volem per a res

    // Prefixat amb ## posem la implementació de l'observbilitat, obtinguda de BaseAdapter

    // ## private final DataSetObservable mDataSetObservable = new DataSetObservable();

    public void registerDataSetObserver(DataSetObserver observer) {
    //    ##mDataSetObservable.registerObserver(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
    //    ## mDataSetObservable.unregisterObserver(observer);
    }
    public void notifyDataSetChanged() {
     //   ## mDataSetObservable.notifyChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1; // @@@@
    }

    @Override
    public boolean isEmpty() {
        return getCount()==0;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }
}
